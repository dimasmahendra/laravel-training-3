<?php

namespace App\Http\Controllers;

use App\Models\Country;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function seeder()
    {
        $client = new Client();
        $response = $client->get('https://restcountries.eu/rest/v2/all');
        
        $body = $response->getBody();
        $data = json_decode($body);

        return $data;
    }  
    
    public function getCountries(Request $request)
    {
        $countries = Country::get();
        return view('user.countries', compact('countries'));
    }
}
