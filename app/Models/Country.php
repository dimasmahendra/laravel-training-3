<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name', 'alpha2_code', 'alpha3_code', 'calling_code', 'demonym', 'flag'];
}
