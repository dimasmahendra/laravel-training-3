<?php

use Faker\Generator as Faker;

$gender = $faker->randomElement(['male', 'female']);
$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name($gender),
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'phone_number' => $faker->phoneNumber,
        'gender' => $gender,
        'country_id' => $faker->address,
        'password' => bcrypt('secret'),
        'created_at' => $faker->address,
        'updated_at' => $faker->address
    ];
});
