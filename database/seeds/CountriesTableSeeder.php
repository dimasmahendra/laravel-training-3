<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = app('App\Http\Controllers\CountryController')->seeder();
        Country::truncate();
        foreach ($countries as $country) {
            $data = [
                'name' => $country->name,
                'alpha2_code' => $country->alpha2Code,
                'alpha3_code' => $country->alpha3Code,
                'calling_code' => $country->callingCodes[0],
                'demonym' => $country->demonym,
                'flag' => $country->flag
            ];

            Country::create($data);
        }
    }
}
