<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        User::truncate();
    	for($i = 1; $i <= 50; $i++){

    		$gender = $faker->randomElement(['M', 'F']);
			
			$data = [
                'first_name' => $faker->firstName,
				'last_name' => $faker->lastName,
				'email' => $faker->safeEmail,
				'phone_number' => $faker->phoneNumber,
				'gender' => $gender,
				'country_id' =>rand(1,250),
				'password' => bcrypt('secret'),
				'created_at' => now(),
				'updated_at' => now()
            ];
            User::create($data);

    	}
    }
}
