@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content')
<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">{{ __('Update Profile') }}</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<form class="form-horizontal" method="POST" action="{{ route('admin/update') }}">
		<div class="box-body">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">{{ __('First Name') }}</label>
				<div class="col-sm-10">
					<input id="name" type="text" 
					class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name', $user->first_name) }}" required autofocus>

					@if ($errors->has('first_name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('first_name') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">{{ __('Last Name') }}</label>
				<div class="col-sm-10">
					<input id="name" type="text" 
					class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" 
					value="{{ old('last_name', $user->last_name) }}" required autofocus>

					@if ($errors->has('last_name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('last_name') }}</strong>
						</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">{{ __('E-Mail Address') }}</label>
				<div class="col-sm-10">
					<input id="email" type="email" 
					class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $user->email) }}" required>

					@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
			</div>
		</div>
		<input type="hidden" name="id" value="{{ old('id', $user->id) }}">
		<!-- /.box-body -->
		<div class="box-footer">
			<button type="submit" class="btn btn-info pull-right">{{ __('Save') }}</button>
		</div>
		<!-- /.box-footer -->
	</form>
</div>
@endsection
