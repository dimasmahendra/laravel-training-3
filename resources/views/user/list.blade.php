@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">User List</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    <th>Country</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $user['first_name'] }}</td>
                    <td>{{ $user['last_name'] }}</td>
                    <td>{{ $user['email'] }}</td>
                    <td>{{ $user['phone_number'] }}</td>
                    <td>{{ $user['gender'] }}</td>
                    <td>{{ $user['country_id'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
<!-- /.box-body -->
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.data-table').dataTable();
        });
    </script>
@stop
