@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Countries List</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Alpha Code 1</th>
                    <th>Alpha Code 2</th>
                    <th>Calling Code</th>
                    <th>Demonym</th>
                    <th>Flag</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($countries as $country)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $country['name'] }}</td>
                    <td>{{ $country['alpha2_code'] }}</td>
                    <td>{{ $country['alpha3_code'] }}</td>
                    <td>{{ $country['calling_code'] }}</td>
                    <td>{{ $country['demonym'] }}</td>
                    <td>
                        <img src="{{ $country['flag'] }}" alt="Smiley face" height="42" width="42">
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
<!-- /.box-body -->
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('.data-table').dataTable();
        });
    </script>
@stop
