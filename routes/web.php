<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/seeder', 'CountryController@seeder');

Route::get('admin/profile', 'UserController@profile');
Route::post('admin/update', 'UserController@update')->name('admin/update');
Route::get('admin/user', 'UserController@getProfile');
Route::get('admin/country', 'CountryController@getCountries');
